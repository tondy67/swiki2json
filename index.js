//
"use strict"

const ts = require('abv-ts')('abv:swiki');
const fs = require('fs');

let bdir, files, file, a, json, s,name;
let out = [], aa = [];
const max = 16;

bdir = 'data/AA';
files = fs.readdirSync(bdir);
for (let it of files) aa.push(bdir+'/'+it);
bdir = 'data/AB';
files = fs.readdirSync(bdir);
for (let it of files) aa.push(bdir+'/'+it);
ts.warn('Files',aa.length);
const part = aa.length / max + 1;

for (let i=0;i<max;i++){
	out = [];
	files = aa.splice(0,part);
	for (let it of files){
		file = fs.readFileSync(it,'utf8');
		a = file.split('\n');
		for (let cur of a){
			json = ts.fromString(cur.trim());
			if(json) out.push(json);
		}
	}
	ts.debug('Titles',out.length);
	name = 'swiki'+i+'.json';
	s = ts.toString(out);
	fs.writeFileSync('data/'+name, s, 'utf8');
	ts.info(name,s.length);
}

//ts.error(aa.length);
