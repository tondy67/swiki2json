#!/bin/bash

mkdir data
wget https://dumps.wikimedia.org/simplewiki/latest/simplewiki-latest-pages-articles.xml.bz2
git clone https://github.com/attardi/wikiextractor.git
cd wikiextractor
python3 WikiExtractor.py -o ../data/ -l --json --no_templates --processes 8 ../simplewiki-latest-pages-articles.xml.bz2
cd ..
rm -rf wikiextractor
